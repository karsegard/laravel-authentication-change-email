<?php

namespace KDA\Tests\Unit;

use Carbon\Carbon;
use Illuminate\Auth\Events\Attempting;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Authentication\Facades\AuthManager;
use KDA\Tests\Models\User;
use KDA\Tests\TestCase;
use Illuminate\Support\Facades\Event;
use Illuminate\Http\Request;
use Illuminate\Testing\TestResponse;
use Illuminate\Routing\Pipeline;
use Illuminate\Routing\Route as RoutingRoute;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use KDA\Laravel\Authentication\AuthManager as AuthenticationAuthManager;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    public function authControllerVerifiedRegister($request)
    {
        AuthManager::createUserUsing(function ($request) {
            return User::create($request->all());
        })
        ->createVerificationUrlUsing(function ($user) {
            return 'https://whatever.com/' . $user->getKey() . "/" . sha1($user->getEmailForVerification());
        })
        ->validateRegistrationUsing(function ($request, $flow_key) {
            return Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ])->validate();
        });
        AuthManager::register($request);
        return AuthManager::getRegisterResponse();
    }

    public function handleVerification($request)
    {
       /* AuthManager::createUserUsing(function ($request) {
            return User::create($request->all());
        })
        ->createVerificationUrlUsing(function ($user) {
            return 'https://whatever.com/' . $user->getKey() . "/" . sha1($user->getEmailForVerification());
        })
        ->validateRegistrationUsing(function ($request, $flow_key) {
            return Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ])->validate();
        });
        AuthManager::register($request);
        return AuthManager::getRegisterResponse();*/
      //  dump(auth()->user());
      //  dump($request->get('id'));
       // dump($request->get('hash'));
        return AuthManager::verifyEmail($request)
        ->getVerifiedResponse();
    }




    /** @test */
    public function it_cant_change_email_user()
    {

        $request = Request::create('/register', 'POST', [
            'name' => 'Taylor Otwell',
            'email' => 'taylor@laravel.com',
            'password' => 'secret-password',
            'password_confirmation' => 'secret-password',
        ], [], [], [
            'HTTP_ACCEPT' => 'application/json',
        ]);

        Notification::fake();

        $response = $this->handleRequestUsing($request, function ($request) {
            return $this->authControllerVerifiedRegister($request);
        });
       

        $response->assertCreated();
        $this->assertDatabaseHas('users', [
            'name' => 'Taylor Otwell',
            'email' => 'taylor@laravel.com',
        ]);
        $user = User::first();
        $validationUrl = null;
        Notification::assertSentTo(
            $user,
            VerifyEmail::class,
            function ($notification, $channels) use ($user,&$validationUrl) {
                $mailData = $notification->toMail($user);
                $this->assertStringContainsString('https://whatever.com', $mailData->actionUrl);
                $validationUrl = str_replace('https://whatever.com/','',$mailData->actionUrl);
                /* $this->assertStringContainsString("Start date: {$subscriptionData['start_date']}", $mailData->render());
         */
                
                return true;
            }
        );

        $test = $this->actingAs($user);
        [$id,$hash]=explode('/',$validationUrl);
        $request = new Request([], [], ['id' => $id,'hash'=>$hash]);

        $request->setRouteResolver(function () use ($request) {
            return (new RoutingRoute('GET', '/{id}/{hash}', []))->bind($request);
        })->setUserResolver(function() use ($user){
            return $user;
        });

        $response = $this->handleRequestUsing($request, function ($request) {
            return $this->handleVerification($request);
        });

        $this->assertNotNull($user->fresh()->email_verified_at);
    }

    
}
