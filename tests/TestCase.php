<?php

namespace KDA\Tests;

use KDA\Laravel\Authentication\ChangeEmail\ServiceProvider as ServiceProvider;
use Orchestra\Testbench\TestCase as ParentTestCase;
use Illuminate\Foundation\Testing\RefreshDatabaseState;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Http\Request;
use Illuminate\Testing\TestResponse;
use Illuminate\Routing\Pipeline;
use KDA\Laravel\Authentication\ServiceProvider as AuthenticationServiceProvider;

class TestCase extends ParentTestCase
{

  public function setUp(): void
  {
    parent::setUp();
  }

  protected function getPackageProviders($app)
  {
    return [
      AuthenticationServiceProvider::class,
      ServiceProvider::class
    ];
  }


  protected function runPackageMigrations($app)
  {
    $provider = ServiceProvider::class;
    $traits = class_uses_recursive($provider);

    if (in_array('KDA\Laravel\Traits\HasLoadableMigration', $traits)) {
     
      $this->artisan('migrate', ['--database' => 'mysql'])->run();
      $this->beforeApplicationDestroyed(function () {
        $this->artisan('migrate:rollback', ['--database' => 'mysql'])->run();
        RefreshDatabaseState::$migrated = false;

      });
    }
  }
  protected function callAfterResolving($name, $callback)
  {
      $this->app->afterResolving($name, $callback);

      if ($this->app->resolved($name)) {
          $callback($this->app->make($name), $this->app);
      }
  }

  protected function loadTestMigrations($paths)
  {
      $this->callAfterResolving('migrator', function ($migrator) use ($paths) {
          foreach ((array) $paths as $path) {
              $migrator->path($path);
          }
      });
  }


  protected function getEnvironmentSetUp($app)
  {
    $this->app = $app;
    $this->loadTestMigrations([__DIR__.'/database/migrations']);
    $this->runPackageMigrations($app);
   
  }
   /**
     * Handle Request using the following pipeline.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  callable  $callback
     * @return \Illuminate\Testing\TestResponse
     */
    protected function handleRequestUsing(Request $request, callable $callback)
    {
        return new TestResponse(
            (new Pipeline($this->app))
                ->send($request)
                ->through([
                    \Illuminate\Session\Middleware\StartSession::class,
                ])
                ->then($callback)
        );
    }
}
