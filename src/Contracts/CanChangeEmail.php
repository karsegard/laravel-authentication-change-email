<?php

namespace  KDA\Laravel\Authentication\ChangeEmail\Contracts;


interface CanChangeEmail
{


    public function sendNewEmailVerificationNotification() ;

    public function markNewEmailAsVerified();

    public function getNewEmailForVerification();
    
    public function hasVerifiedNewEmail();

}
