<?php

namespace KDA\Laravel\Authentication\ChangeEmail;

use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\Traits\HasConfig;
use KDA\Laravel\Traits\HasMigration;

class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasConfig;
    use HasMigration;
    protected $packageName = 'laravel-authentication-change-email';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    // trait \KDA\Laravel\Traits\HasConfig; 
    //    registers config file as 
    //      [file_relative_to_config_dir => namespace]
    protected $configDir = 'config';
    protected $configs = [
        'kda/authentication-change-email.php'  => 'kda.authentication-change-email'
    ];
    public function register()
    {
        parent::register();
    }
    /**
     * called after the trait were registered
     */
    public function postRegister()
    {
    }
    //called after the trait were booted
    protected function bootSelf()
    {
    }
}
