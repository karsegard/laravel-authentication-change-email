<?php

namespace  KDA\Laravel\Authentication\ChangeEmail\Concerns;

use Illuminate\Auth\Notifications\VerifyEmail;

trait CanChangeEmail
{
    public function sendNewEmailVerificationNotification(){
        $this->notify(new VerifyEmail);
    }

    public function markNewEmailAsVerified(){

        return $this->forceFill([
            'previous_email' => $this->email,
            'email'=>$this->new_email,
            'new_email'=>null,
            'new_email_verified_at'=>now()
        ])->save();
    }

    public function getNewEmailForVerification(){
        return $this->new_email;
    }
    
    public function hasVerifiedNewEmail(){
        return ! is_null($this->new_email_verified_at);
    }

}
